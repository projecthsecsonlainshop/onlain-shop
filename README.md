# Онлайн-блог

Маликова Анастасия, гр. 143

## Что сделано

 * Витрина магазина с категориями
 * Корзина пользователя, в которую можно добавлять или удалять товары
 * После оформления заказа, письмо с данными о заказе и пользователе отправляется на почту владельца магазина
 * Приложение защищено от инъекций к базе

ИТОГО: минимальная функциональность на 4 балла
 
## Системные требования

 * Python версии не ниже 3.4
 * Django версии не ниже 1.9

## Настройка и запуск

В репозитории есть файл с примером каталога товаров `db.sqlite3`. Для создания чистой установки нужно удалить этот файл, и создать чистую базу данных:
```
rm db.sqlite3
python3 manage.py makemigrarions
python3 manage.py migrate
```

Для запуска сервера нужно ввести команду:
```
python3 manage.py runserver
```

После этого сервер будет доступен по адресу `http://localhost:8000`

## Добавление товаров 

Добавление товаров реализовано штатными средствами Django. Для этого необходимо создать пользователя-администратора: 
```
python3 manage.py createsuperuser
```
После этого войти в администраторскую панель по адресу: `http://localhost:8000/admin`

## Отправка сообщений владельцу магазина

Для отправки сообщений владельцу магазина необходимо настроить реквизиты почтового сервера, и адрес владельца магазина. 
Эти настройки находятся в файле `blog/email_config.py`
