from django.contrib import admin

from .models import Good
admin.site.register(Good)

from .models import Categories
admin.site.register(Categories)

from .models import CategoriesGood
admin.site.register(CategoriesGood)

from .models import User
admin.site.register(User)

from .models import Payment
admin.site.register(Payment)

from .models import Order
admin.site.register(Order)

from .models import Cart
admin.site.register(Cart)

from .models import Store
admin.site.register(Store)

from .models import StoreGood
admin.site.register(StoreGood)

from .models import Kupon
admin.site.register(Kupon)