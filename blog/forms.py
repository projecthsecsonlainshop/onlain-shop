from django import forms

class RegForm(forms.Form):
    fname = forms.CharField(label='Имя', max_length=50)
    lname = forms.CharField(label='Фамилия', max_length=50)
    login = forms.CharField(label='Логин', max_length=20)
    email = forms.EmailField()
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput,
                               max_length=10)
class LogIn(forms.Form):
    login = forms.CharField(label='Логин', max_length=20)
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput,
                               max_length=10)
class OrderForm(forms.Form):
    address = forms.CharField(label = 'Адрес', max_length = 25)
    phone_number = forms.CharField(label = 'Номер телефона', max_length = 11)
    
