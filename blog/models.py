from django.db import models
from django.utils import timezone

class Good(models.Model):
    #good_id = models.ForeignKey(Good)
    price = models.IntegerField(default = 0)
    brand = models.CharField(max_length = 20)
    name = models.CharField(max_length = 20)
    item = models.IntegerField(default = 0)
    info = models.TextField()


class Categories(models.Model):
    #categories_id = models.ForeignKey(Categories)
    name = models.CharField(max_length = 20)

class CategoriesGood(models.Model):
    good_id = models.ForeignKey(Good)
    categories_id = models.ForeignKey(Categories)
    def __str__(self):
        return str(self.categories_id)

class User(models.Model):
    #user_id = models.ForeignKey(User)
    fname = models.CharField(max_length = 50)
    lname = models.CharField(max_length = 50)
    login = models.CharField(max_length = 20)
    password = models.CharField(max_length = 10)
    email = models.CharField(max_length = 30)

class Cart(models.Model):
    good = models.ForeignKey(Good)
    user_id = models.ForeignKey(User)
    #cart_id = models.ForeignKey(Cart)
    quantity = models.IntegerField(default = 0)

class Order(models.Model):
    user_id = models.ForeignKey(User)
    address = models.CharField(max_length = 25)
    phone_number = models.CharField(max_length = 11)

class Payment(models.Model):
    #payment_id = models.ForeignKey(payment)
    user_id = models.ForeignKey(User)
    order = models.ForeignKey(Order)

class Store(models.Model):
    #store_id = models.ForeignKey(store)
    address = models.CharField(max_length = 100)
    working_hours = models.CharField(max_length = 15)

class StoreGood(models.Model):
    store_id = models.ForeignKey(Store)
    good_id = models.ForeignKey(Good)
    quantity = models.IntegerField(default = 0)
class Kupon(models.Model):
    good_id = models.ForeignKey(Good)
    end_date = models.DateField()
    discount = models.IntegerField(default = 0)
