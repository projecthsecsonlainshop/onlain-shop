from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.shortcuts import get_object_or_404, render
from .forms import RegForm, LogIn, OrderForm
from django.core.mail import send_mail
from .models import Good, Categories, CategoriesGood, User, Cart, Order
import smtplib
from smtplib import SMTP
from email.mime.text import MIMEText
from email.header import Header
import blog.email_config as email_config

def index(request):
    login = ''
    if 'login' in request.session:
        user_list = User.objects.filter(login = request.session['login'])
        if user_list:
            login = user_list[0].login
    goods_list = Good.objects.all()
    categories_list = Categories.objects.all()
    template = loader.get_template('blog/index.html')
    form = RegForm()
    log = LogIn()
    context = {
        'goods': goods_list,
        'categories' : categories_list,
        'log': log,
        'login': login,
    }
    return HttpResponse(template.render(context, request))

def detail(request, good_id):
    login = ''
    if 'login' in request.session:
        user_list = User.objects.filter(login = request.session['login'])
        if user_list:
            login = user_list[0].login
    good = get_object_or_404(Good, pk=good_id)
    return render(request, 'blog/detail.html', {'good': good, 'login':login})

def order(request):
    login = ''
    if 'login' in request.session:
        user_list = User.objects.filter(login = request.session['login'])
        if user_list:
            login = user_list[0].login
    return render(request, 'blog/order.html', {'login':login})

def categories_list(request, categories_id):
    login = ''
    if 'login' in request.session:
        user_list = User.objects.filter(login = request.session['login'])
        if user_list:
            login = user_list[0].login
    good_list = []
    category_name = get_object_or_404(Categories, pk = categories_id).name
    goods_list1 = CategoriesGood.objects.filter(categories_id = categories_id)
    for item in goods_list1:
        good  = get_object_or_404(Good, pk = item.good_id.id)
        good_list.append(good)
    template = loader.get_template('blog/category.html')
    context = {
            'goods': good_list,
            'category_name': category_name,
            'login':login
        }
    return HttpResponse(template.render(context, request))

def reg(request):
    if request.method == 'POST':
        form = RegForm(request.POST)
        if form.is_valid():
            user = User(fname=form.cleaned_data['fname'],
                        lname=form.cleaned_data['lname'],
                        login=form.cleaned_data['login'],
                        email=form.cleaned_data['email'],
                        password=form.cleaned_data['password'])
            user.save()
            request.session['login'] = form.cleaned_data['login']
            return HttpResponseRedirect('/')
def log(request):
    if request.method == 'POST':
        form = LogIn(request.POST)
        if form.is_valid():
            user_list = User.objects.filter(login = form.cleaned_data['login'],
                                            password = form.cleaned_data['password'])
            if user_list:
                request.session['login'] = form.cleaned_data['login']
                return HttpResponseRedirect('/')
            else:
                return HttpResponse('Bad Login')
        else: return HttpResponse('ЗАПОЛНИТЕ ПОЛЯ АВТОРИЗАЦИИ')

def registration(request):
    template = loader.get_template('blog/reg.html')
    context = {
        'reg': RegForm(),
    }
    return HttpResponse(template.render(context, request))

def add_to_cart(request, good_id):
    good = Good.objects.filter(pk = good_id)[0]
    user = User.objects.filter(login = request.session['login'])[0]
    cart = Cart(good = good, user_id = user, quantity = 1)
    cart.save()
    return HttpResponseRedirect('/blog/' + str(good_id))

def show_cart(request):
    login = ''
    if 'login' in request.session:
        user_list = User.objects.filter(login = request.session['login'])
        if user_list:
            login = user_list[0].login
    template = loader.get_template('blog/cart.html')
    user = User.objects.filter(login = request.session['login'])[0]
    cart_list = Cart.objects.filter(user_id = user)
    good_list = []
    for item in cart_list :
        good  = get_object_or_404(Good, pk = item.good.id)
        good_list.append(good)
    context = {
        'goods'  : good_list,
        'login' : login
    }
    return HttpResponse(template.render(context, request))

def order(request):
    login = ''
    if 'login' in request.session:
        user_list = User.objects.filter(login = request.session['login'])
        if user_list:
            login = user_list[0].login
    template = loader.get_template('blog/order.html')
    context = {
        'order': OrderForm(),
        'login' : login
    }
    return HttpResponse(template.render(context, request))

def send_mail(fromm, to, subject, body):
    msg = MIMEText(body, "plain", "utf-8")
    msg["Subject"] = Header(subject, "utf-8")
    data = msg.as_string()
    srv = SMTP(SERVER)
    srv.starttls()
    srv.login(USER, PASSWORD)
    srv.sendmail(fromm, to, data)
    srv.quit()

def order_making(request):
    login = ''
    if 'login' in request.session:
        user_list = User.objects.filter(login = request.session['login'])
        if user_list:
            login = user_list[0].login
    template = loader.get_template('blog/order_making.html')
    user = User.objects.filter(login = request.session['login'])[0]
    if request.method == 'POST':
        form = OrderForm(request.POST)
        if form.is_valid():
            order = Order(user_id=user,
                        address=form.cleaned_data['address'],
                        phone_number=form.cleaned_data['phone_number'])
            order.save()
            cart_list = Cart.objects.filter(user_id = user)
            good_list = []
            s = 0
            MESSAGE= "Товары :"
            for item in cart_list :
                good  = get_object_or_404(Good, pk = item.good.id)
                good_list.append(good)
                s = s + good.price
                MESSAGE = MESSAGE + good.name + ' ' + str(good.price) + ' \n'
            MESSAGE = MESSAGE + ' ' + user.fname + ' ' + user.lname + ' ' + order.address + ' ' + str(s)
            context = {
                'goods'  : good_list,
                'user' : user,
                'order' : order,
                'sum' : s,
                'login' : login
            }
            fromaddr = email_config.SEND_EMAIL_ACCOUNT_FROM
            toaddrs  = email_config.ADMIN_EMAIL_ADDRESS


            msg = MIMEText(MESSAGE, "plain", "utf-8")
            msg["Subject"] = Header("Заказ", "utf-8")
            data = msg.as_string()
            srv = SMTP(email_config.SMTP_SERVER + ":" + str(email_config.SMTP_PORT))
            if email_config.SMTP_USE_SSL:
                srv.starttls()
            srv.login(email_config.SEND_EMAIL_ACCOUNT_FROM, email_config.SEND_EMAIL_ACCOUNT_PASSWORD)
            srv.sendmail(fromaddr, toaddrs, data)
            srv.quit()
            #send_mail('заказ',msg,fromaddr,[toaddrs],fail_silently=False,auth_user = 'ordertoblog', auth_password ='qwerty1488')
            Cart.objects.filter(user_id = user).delete()
            return HttpResponse(template.render(context, request))

def delete_cart(request, good_id):
    login = ''
    if 'login' in request.session:
        user_list = User.objects.filter(login = request.session['login'])
        if user_list:
            login = user_list[0].login
    good = Good.objects.get(pk = good_id)
    user = User.objects.filter(login = request.session['login'])[0]
    cart = Cart.objects.filter(good = good, user_id = user)[0]
    newgood = get_object_or_404(Good, pk = cart.good.id)
    name = newgood.name
    cart.delete()
    return HttpResponse('удалили ' + str(name))

def exit(request):
    try:
        del request.session['login']
    except KeyError:
        pass
    return HttpResponseRedirect('/')