from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^blog/(?P<good_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^blog/detail/buy/(?P<good_id>[0-9]+)$', views.add_to_cart, name='buy'),
    url(r'^blog/category/(?P<categories_id>[0-9]+)/$', views.categories_list, name='category'),
    url(r'^blog/reg$', views.reg, name='reg'),
    url(r'^blog/log$', views.log, name='log'),
    url(r'^blog/registration$', views.registration, name='registration'),
    url(r'^blog/cart$', views.show_cart, name='cart'),
    url(r'^blog/order$', views.order, name='order'),
    url(r'^blog/order_making$', views.order_making, name='order_making'),
    url(r'^blog/delete_cart/(?P<good_id>[0-9]+)/$', views.delete_cart, name='delete_cart'),
    url(r'^blog/exit$', views.exit, name='exit'),

]
